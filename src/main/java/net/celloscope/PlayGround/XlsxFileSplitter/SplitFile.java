package net.celloscope.PlayGround.XlsxFileSplitter;


import java.awt.font.NumericShaper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


@Slf4j
public class SplitFile {
    private final String fileName;
    private final int maxRows;
    private final String path;
    private final String userfilename = "";
    public static int filecount;
    public static String taskname;
    public static int rowcounter;

    public SplitFile(String fileName, final int maxRows, String filepath, String userfilename) throws FileNotFoundException {
        path = filepath;
        taskname = userfilename;
        this.fileName = fileName;
        this.maxRows = maxRows;

        File file = new File(fileName);
        FileInputStream inputStream = new FileInputStream(file);
        try {
            Workbook workbook = new XSSFWorkbook(inputStream);
            XSSFSheet sheet = (XSSFSheet) workbook.getSheetAt(0);
            log.info("Sheet Opened");
            /* Only split if there are more rows than the desired amount. */
            if (sheet.getPhysicalNumberOfRows() >= maxRows) {
                List<SXSSFWorkbook> wbs = splitWorkbook(workbook);
                writeWorkBooks(wbs);
            }

        } catch (EncryptedDocumentException | IOException e) {
            e.printStackTrace();
        }
    }

    private List<SXSSFWorkbook> splitWorkbook(Workbook workbook) {

        List<SXSSFWorkbook> workbooks = new ArrayList<>();

        SXSSFWorkbook wb = new SXSSFWorkbook();
        SXSSFSheet sh = (SXSSFSheet) wb.createSheet();



        String headCellarr[] = new String[50];

        int rowCount = 0;
        int colCount = 0;
        int headflag = 0;
        int rcountflag = 0;
        int titleColumns = 0;
        int startingPageCounter = 1;

        XSSFSheet sheet = (XSSFSheet) workbook.getSheetAt(0);


        // header create

        int j = 0;
        for (Cell cell : sheet.getRow(0)) {
            headCellarr[j] = cell.toString();
            j++;
        }
        titleColumns = j;

        SXSSFRow newRow;
        SXSSFCell newCell;

        for (Row row : sheet) {

            if (rowCount > maxRows) {
                headflag = 1;
                workbooks.add(wb);
                wb = new SXSSFWorkbook();
                sh = (SXSSFSheet) wb.createSheet();
                rowCount = 0;
            }


            if (headflag == 1) {

                log.info("HeadFlag ------------------------ 1");

                newRow = (SXSSFRow) sh.createRow(rowCount++);
                headflag = 0;
                for (int k = 0; k < titleColumns; k++) {
                    newCell = (SXSSFCell) newRow.createCell(colCount++);
                    newCell.setCellValue(headCellarr[k]);
                }
                colCount = 0;
                newRow = (SXSSFRow) sh.createRow(rowCount++);
                for (Cell cell : row) {
                    newCell = (SXSSFCell) newRow.createCell(colCount++);

                    log.info("CellType == {}",cell.getCellType());


                    if (cell.getCellType() == CellType.BLANK) {
                        newCell.setCellValue("-");
                    } else {
                        newCell = setValue(newCell, cell);
                    }
                }
                colCount = 0;
            } else {
                rowcounter++;
                newRow = (SXSSFRow) sh.createRow(rowCount++);
                for (int cn = 0; cn < row.getLastCellNum(); cn++) {
                    Cell cell = row.getCell(cn, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                    newCell = (SXSSFCell) newRow.createCell(cn);
                    newCell.setCellValue(cell.getStringCellValue());
                }
            }
        }


        /* Only add the last workbook if it has content */
        if (wb.getSheetAt(0).getPhysicalNumberOfRows() > 0) {
            workbooks.add(wb);
        }
        return workbooks;
    }

    private SXSSFCell setValue(SXSSFCell newCell, Cell cell) {
        switch (cell.getCellType()) {
            case STRING -> {
//                newCell.setCellValue(cell.getRichStringCellValue().getString());
                newCell.setCellValue(cell.getStringCellValue());
                log.info("=================STRING");
            }
            case NUMERIC -> {
                if (DateUtil.isCellDateFormatted(cell)) {
                    newCell.setCellValue(cell.getDateCellValue());
                } else {
                    //newCell.setCellValue(cell.getNumericCellValue());
                    newCell.setCellValue(cell.toString());
                }
                log.info("NUMERIC");
            }
            case BOOLEAN -> {
                newCell.setCellValue(cell.getBooleanCellValue());
                log.info("BOOLEAN");
            }
            case FORMULA -> {
                newCell.setCellFormula(cell.getCellFormula());
                log.info("FORMULA");
            }
            case BLANK -> newCell.setCellValue("-");
            default -> {
                log.info("Could not determine cell type");
                newCell.setCellValue(cell.toString());
            }
        }
        return newCell;
    }

    /* Write all the workbooks to disk. */
    private void writeWorkBooks(List<SXSSFWorkbook> wbs) {
        FileOutputStream out;
        boolean mdir = new File(path + "/split").mkdir();

        try {
            for (int i = 0; i < wbs.size(); i++) {
                String newFileName = fileName.substring(0, fileName.length() - 5);
                //out = new FileOutputStream(new File(newFileName + "_" + (i + 1) + ".xlsx"));
                out = new FileOutputStream(new File(path + "/split/" + taskname + "_" + (i + 1) + ".xlsx"));
                wbs.get(i).write(out);
                out.close();
                log.info("written File == {}",i);
                filecount++;
            }
            log.info("user FileName == {}",userfilename);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static void main(String[] args) throws FileNotFoundException {
        // This will create a new workbook every 200 rows.
        // new Splitter(filename.xlsx, No of split rows, filepath, newfilename);
        new SplitFile("src/main/resources/static/EFT-B001-20221010-010272686.xlsx", 300, "src/main/resources/static", "newfilename");  //No of rows to split: 10 K
    }

}
